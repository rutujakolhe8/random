package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Portfolio;

@Repository
public class MySQLPortfolioRepository implements PortfolioRepository {
	
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Portfolio> getAllPortfolio() {
		// TODO Auto-generated method stub
		String sql = "SELECT ID, stockTicker, price, volume FROM Portfolio";
		return template.query(sql, new PortfolioRowMapper());

	}

	@Override
	public Portfolio add_Portfolio(Portfolio Portfolio) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO Portfolio(ID, stockTicker, price, volume) " +
				"VALUES(?,?,?,?)";
		template.update(sql, Portfolio.getId(), Portfolio.getStockTicker(), Portfolio.getPrice(), Portfolio.getVolume() );
		return Portfolio;
	}

	@Override
	public int delete_Portfolio(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Portfolio WHERE ID = ?";
		template.update(sql,id);
		return id;
	}
	
	

}

class PortfolioRowMapper implements RowMapper<Portfolio>{

	@Override
	public Portfolio mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Portfolio(rs.getInt("ID"),
				rs.getString("StockTicker"),
				rs.getDouble("Price"),
				rs.getInt("Volume"));
	}
	
}

