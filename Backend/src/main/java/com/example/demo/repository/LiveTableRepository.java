package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.LiveTable;

@Component
public interface LiveTableRepository {
	public List<LiveTable> getAllLiveTable();
	public LiveTable getLiveTableByname(String id);

}
