package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.LiveTable;
import com.example.demo.entities.Trading;
import com.example.demo.service.LiveTableService;

@RestController
@CrossOrigin
@RequestMapping("api/LiveTable")
public class LiveTableController {
	
	@Autowired
	LiveTableService service;
	
	@GetMapping(value = "/")
	public List<LiveTable> getAllLiveTable() {
		return service.getAllLiveTable();
	}
	
	@GetMapping(value = "/{id}")
	public LiveTable getLiveTableByname(@PathVariable("id") String id) {
	  return service.getLiveTableByname(id);
	}


}
